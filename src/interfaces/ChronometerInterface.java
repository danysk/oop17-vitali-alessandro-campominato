package interfaces;

/**
 * interfaccia di Chronometer
 * 
 * @author Alessandro
 *
 */
public interface ChronometerInterface {
	
  /**
   * riavvia il cronometro
   */
	public void restartChronometer();
	
	/**
	 * 
	 * @return se il cronometro � gi� avviato
	 */
	public boolean hasTimerStarted();
	
	/**
	 * 
	 * @return il tempo impiegato in secondi
	 */
	public int getTime();

}
