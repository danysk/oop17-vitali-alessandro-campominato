package interfaces;

/**
 * interfaccia di Difficult
 * 
 * @author Alessandro
 *
 */
public interface DifficultInterface {

  /**
   * imposta la difficolt� facile
   */
	public void setDifficultEasy();
	
	/**
	 * imposta la difficolt� media
	 */
	public void setDifficultMedium();
	
	/**
	 * imposta la difficolt� difficile
	 */
	public void setDifficultHard();
	
	/**
	 * imposta una difficolt� personalizzata con righe, colonne e bombe scelte dall'utente
	 * 
	 * @param rows
	 *     il numero di righe desiderato
	 * @param columns
	 *     il numero di colonne desiderato
	 * @param bombs
	 *     il numero di bombe desiderato
	 */
	public void setDifficultCustom(int rows, int columns, int bombs);
	
	/**
	 * 
	 * @return la difficolt� attualmente impostata
	 */
	public String getDifficult();
	
	/**
	 * 
	 * @return il numero di righe
	 */
	public int getRows();
	
	/**
	 * 
	 * @return il numero di colonne
	 */
	public int getColumns();
	
	/**
	 * 
	 * @return il numero di bombe
	 */
	public int getBombs();
	
	/**
	 * 
	 * @return se la difficolt� � cambiata
	 */
	public boolean isChange();
	
	/**
	 * imposta la difficolt� non cambiata
	 */
	public void setNotChange();
	
}
