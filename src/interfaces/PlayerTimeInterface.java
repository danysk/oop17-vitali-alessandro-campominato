package interfaces;

/**
 * interfaccia di PlayerTime
 * 
 * @author Alessandro
 *
 */
public interface PlayerTimeInterface {
	
  /**
   * 
   * @return il tempo del giocatore
   */
	public int getTime();
	
	/**
	 * 
	 * @return il nome del giocatore
	 */
	public String getName();

}
